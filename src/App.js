import React from 'react';
import styles from './App.module.css';
import { ChakraProvider, Text } from "@chakra-ui/react"
import { Board } from './features/board/Board';

function App() {
  return (
    <>
      <header className={styles.header}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <img width="50px" src="/images/logo.png" alt="logo" loading="lazy" />
          <div className={styles.headline}>
            <a href="https://bitbucket.org/buseet/frontend-tasks/src/master/" target="_blank" rel="noreferrer"><Text fontSize="2xl">Buseet Senior Front-end Engineer Task</Text></a>
            <Text fontSize="lg">task management board Application</Text>
          </div>
        </div>
        <div style={{ marginLeft: "auto" }}>
          <a style={{ display: 'flex', alignItems: 'center' }} href="https://www.linkedin.com/in/mostafaabobakr/" target="_blank" rel="noreferrer">
            <Text fontSize="2xl" style={{ marginRight: '10px' }}>Mostafa Abobakr</Text>
            <img width="50px" src="/images/Mostafa_circle-1030x1429.png" alt="mostafa" loading="lazy" />
          </a>
        </div>
      </header>
      <main className={styles.main}>
        <Board />
      </main>
      <footer className={styles.footer}></footer>
    </>
  );
}

function MyApp({ Component }) {
  return (
    <ChakraProvider>
      <App />
    </ChakraProvider>
  )
}
export default MyApp;
