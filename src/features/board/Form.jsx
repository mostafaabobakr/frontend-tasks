import { useState } from 'react';
import { FormControl, Input, Button, InputRightElement, InputGroup } from '@chakra-ui/react';
import { useDispatch } from 'react-redux';
import { addTask } from './boardSlice';
import styles from './Board.module.css';



export function Form() {
  const [value, setValue] = useState('');
  const dispatch = useDispatch();


  const handleChange = (e) => {
    const value = e.target.value;
    setValue(value);
  }

  const handleAddTask = () => {
    if (value !== '') dispatch(addTask(value));
  }

  return (
    <>
      <FormControl id="task">
        <h2>Add Task</h2>
        <div className={styles.inputcontainer}>
          <InputGroup size="md">
            <Input value={value} type="text" placeholder="Task Title" size="lg" onChange={handleChange} />
            <InputRightElement width="5rem" style={{ top: '50%', transform: 'translateY(-50%)' }}>
              <Button
                onClick={handleAddTask}
                colorScheme="teal"
                variant="ghost"
                size="lg"
                style={{ borderRadius: "0px 6px 6px 0px" }}
              >ADD</Button>
            </InputRightElement>
          </InputGroup>
        </div>
      </FormControl>
    </>
  )
}