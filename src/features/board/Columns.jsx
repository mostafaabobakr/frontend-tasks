import { useSelector } from 'react-redux';
import { Droppable } from 'react-beautiful-dnd';

import { selectTasks, selectColumns, } from './boardSlice';
import styles from "./Board.module.css";
import ColumnItems from './ColumnItems';




export default function Columns({ openModal, handleDeleteTask, handleEditTask }) {
  const tasks = useSelector(selectTasks);
  const columnsObj = useSelector(selectColumns);
  const colKeys = Object.keys(columnsObj);

  return (
    colKeys.map((columnId, index) => {
      const col = columnsObj[columnId];

      return (
        <section className={styles.col} key={col.id}>
          <p className={styles.coltitle}
          >{col.title} <span>{(col.taskIds).length}</span></p>
          <Droppable droppableId={col.id} index={index}>
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                className={styles.droppable}
                style={{
                  backgroundColor: snapshot.isDraggingOver ? '#f7d1b6' : 'transparent'
                }}
              >
                <ColumnItems
                  colId={col.id}
                  tasks={tasks}
                  colTasksArr={col.taskIds}
                  openModal={openModal}
                  handleDeleteTask={handleDeleteTask}
                  handleEditTask={handleEditTask}
                />
                {provided.placeholder}
              </div>
            )}
          </Droppable>

        </section>
      )
    })
  )
}