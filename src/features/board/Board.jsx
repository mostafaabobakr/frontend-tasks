import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { DragDropContext, } from 'react-beautiful-dnd';
import { Container } from "@chakra-ui/react";

import { deleteTask, editTask, moveToNewColumn, selectColumns, sortColumn } from './boardSlice';
import Columns from './Columns';
import styles from './Board.module.css';
import { Form } from './Form';
import { ViewTaskModal } from './ViewTask';


export function Board() {
  const columnsObj = useSelector(selectColumns);
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);
  const [modalData, setModalData] = useState({});

  const handleDeleteTask = (taskId, colId) => {
    dispatch(deleteTask({ taskId, colId }));
  }
  const handleEditTask = (taskId, value, desc, colId) => {
    dispatch(editTask({ taskId, value, desc, colId }));
  }

  const openModal = (data) => {
    // 1. set modal data
    setModalData(data);
    // 2. open modal
    setOpen(true);
  }
  const closeModal = () => {
    setOpen(false);
  }


  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    const noDestination = !destination;
    const dropAtSamePlace = destination.droppableId === source.droppableId && destination.index === source.index;
    if (noDestination || dropAtSamePlace) return;
    //
    const startColumn = columnsObj[source.droppableId];
    const endColumn = columnsObj[destination.droppableId];
    const startColumnNewTaskId = [...startColumn.taskIds];
    const endColumnNewTaskId = [...endColumn.taskIds];
    // move task to new index in array "sort within same column"
    // 1. remove the moved task
    startColumnNewTaskId.splice(source.index, 1);
    if (startColumn === endColumn) {
      // 2. insert draggableId in destination.index
      startColumnNewTaskId.splice(destination.index, 0, draggableId);
      // 3. create new column
      const newColumn = {
        ...startColumn,
        taskIds: startColumnNewTaskId
      }
      // 4. modify 'board' state in redux
      return dispatch(sortColumn(newColumn));
    } else if (startColumn.id === 'done') {
      // make items from done not movable
      return;
    } else {
      // Moving from one column.taskId to another
      const newStartColumn = {
        ...startColumn,
        taskIds: startColumnNewTaskId
      }
      //  insert draggableId in destination.index
      endColumnNewTaskId.splice(destination.index, 0, draggableId);
      const newEndColumn = {
        ...endColumn,
        taskIds: endColumnNewTaskId
      }
      return dispatch(moveToNewColumn({
        newStartColumn,
        newEndColumn
      }))
    }
  }

  return (
    <Container maxW="container.xl">
      <Form />
      <div className={styles.container}>
        <DragDropContext
          onDragEnd={onDragEnd}>
          <Columns
            openModal={openModal}
            handleDeleteTask={handleDeleteTask}
            handleEditTask={handleEditTask}
          />
        </DragDropContext>
      </div>
      <ViewTaskModal
        isOpen={open}
        data={modalData}
        closeModal={closeModal}
        handleEditTask={handleEditTask} />
    </Container>
  );
}

