import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuid } from 'uuid';

const uuids = {
  1: uuid(),
  2: uuid(),
  3: uuid(),
  4: uuid(),
  5: uuid(),
}

const initialState = {
  tasks: {
    [uuids[1]]: {
      id: uuids[1], title: "Task-1 Engage Jupiter Express for outer solar system travel",
      desc: "desc Engage Jupiter Express for outer solar system travel"
    },
    [uuids[2]]: {
      id: uuids[2], title: "Task-2 Create 90 day plans for all departments in the Mars Office",
      desc: "desc Create 90 day plans for all departments in the Mars Office"
    },
    [uuids[3]]: {
      id: uuids[3], title: "Task-3 Engage Saturn's Rings Resort as a preferred provider",
      desc: "desc Engage Saturn's Rings Resort as a preferred provider"
    },
    [uuids[4]]: {
      id: uuids[4], title: "Task-4 Enable Speedy SpaceCraft as the preferred",
      desc: "desc Enable Speedy SpaceCraft as the preferred"
    },
    [uuids[5]]: {
      id: uuids[5], title: "Task-5 Engage Saturn Shuttle Lines for group tours",
      desc: "desc Engage Saturn Shuttle Lines for group tours"
    }
  },
  columns: {
    "to-do": {
      id: "to-do",
      title: "TO DO",
      taskIds: [uuids[1], uuids[2], uuids[3], uuids[4], uuids[5]]
    },
    "in-progress": {
      id: "in-progress",
      title: "IN PROGRESS",
      taskIds: []
    },
    "done": {
      id: "done",
      title: "DONE",
      taskIds: []
    }
  },
  // facilitate column reordering if needed
  columnsOrder: ["to-do", "in-progress", "done"],
}


export const boardSlice = createSlice({
  name: 'board',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    sortColumn: (state, action) => {
      const newColumn = action.payload;

      state.columns = {
        ...state.columns,
        [newColumn.id]: newColumn
      }

    },
    moveToNewColumn: (state, action) => {
      const { newStartColumn, newEndColumn } = action.payload;

      state.columns = {
        ...state.columns,
        [newStartColumn.id]: newStartColumn,
        [newEndColumn.id]: newEndColumn,
      }

    },

    addTask: (state, action) => {
      const title = action.payload;
      const id = uuid();

      const newTaskValue = {
        id,
        title,
        desc: 'task description text'
      }

      // 1. add to state
      state.tasks = {
        ...state.tasks,
        [id]: newTaskValue
      }
      // 2. add it to the `to-do` column
      const col = state.columns['to-do'];
      let colTasksArr = col.taskIds;

      col.taskIds = [...colTasksArr, id];
    },

    editTask: (state, action) => {
      const { taskId, value: newTitle } = action.payload;
      let TASK = state.tasks[taskId];

      state.tasks = {
        ...state.tasks,
        [taskId]: {
          ...TASK,
          title: newTitle,
          desc: action.payload.desc || TASK.desc
        }
      };
    },

    deleteTask: (state, action) => {
      let TASKS = state.tasks;
      const { taskId, colId } = action.payload;

      // delete from tasks
      const reducedTasksObject = Object.keys(TASKS).reduce((acc, key) => {
        const value = TASKS[key];
        if (key !== taskId) {
          return {
            ...acc,
            [key]: value
          }
        }
        return acc;
      }, {});

      state.tasks = reducedTasksObject;

      // delete from columns
      const col = state.columns[colId]
      let colTasksArr = col.taskIds;

      col.taskIds = colTasksArr.filter((id) => id !== taskId);


    }
  },

});

export const { sortColumn, moveToNewColumn, addTask, editTask, deleteTask } = boardSlice.actions;

export const selectTasks = (state) => state.board.tasks;
export const selectColumns = (state) => state.board.columns;


export default boardSlice.reducer;
