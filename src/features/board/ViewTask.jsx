import { useState } from 'react';
import { useSelector } from 'react-redux';
import { selectTasks } from './boardSlice';
import {
  Button,
  Editable,
  EditableInput,
  EditablePreview,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';


export function ViewTaskModal({ isOpen, data, closeModal, handleEditTask }) {
  const [editedData, setEditedData] = useState({});

  const tasks = useSelector(selectTasks);

  const { taskId, colId } = data;
  const task = tasks[taskId];

  console.log('data:', data);

  return (
    <>
      <Modal isOpen={isOpen} size="xl">
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Click Text to Edit Task</ModalHeader>
          <ModalBody>
            <Text fontSize="sm" color="gray.600">Title:</Text>
            <Editable
              defaultValue={task && task.title}
              onSubmit={(value) => setEditedData((prevState) => ({ ...prevState, title: value }))}
            >
              <EditablePreview style={{ width: "100%" }} />
              <EditableInput />
            </Editable>
            <br />
            <Text fontSize="sm" color="gray.600">Description:</Text>
            <Editable
              defaultValue={task && task.desc}
              onSubmit={(value) => setEditedData((prevState) => ({ ...prevState, desc: value }))}
            >
              <EditablePreview style={{ width: "100%" }} />
              <EditableInput />
            </Editable>
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme='blue' mr={3}
              onClick={() => {
                // update state
                handleEditTask(taskId, editedData.title, editedData.desc, colId);
                // close modal
                closeModal();
              }
              }
            >
              Save
            </Button>
            <Button variant='ghost' onClick={closeModal}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
