import { Draggable } from 'react-beautiful-dnd';

import { IconButton, Tooltip, EditablePreview, EditableInput, Editable } from "@chakra-ui/react";
import { DeleteIcon, ViewIcon } from '@chakra-ui/icons';

import styles from "./Board.module.css";


export default function ColumnItems({ openModal, handleEditTask, handleDeleteTask, colId, colTasksArr, tasks }) {

  return (
    colTasksArr.map((taskID, index) => {
      const task = tasks[taskID];

      if (!task) return null;

      return (
        <Draggable
          draggableId={task.id}
          index={index}
          key={task.id}
          isDragDisabled={colId === 'done' ? true : false}
        >
          {(provided, snapshot) => (
            <div
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              ref={provided.innerRef}
              className={styles.card}
              style={{ backgroundColor: snapshot.isDragging ? 'lightgreen' : 'white', ...provided.draggableProps.style }}
            >
              <Editable
                textAlign="center"
                value={task.title}
                fontSize="2xl"
                isPreviewFocusable={false}
                onSubmit={(value) => handleEditTask(task.id, value)}
              >
                <EditablePreview style={{ cursor: "inherit", width: "100%" }} />
                <EditableInput />
                <div className={styles.cardActionsBtns}>
                  <Tooltip label="View\Edit Task" bg="teal">
                    <IconButton
                      onClick={() => openModal({ taskId: task.id, colId, title: task.title })}
                      variant="ghost"
                      colorScheme="teal"
                      aria-label="View Task"
                      size="sm"
                      fontSize="18px"
                      icon={<ViewIcon />}
                    />
                  </Tooltip>
                  <Tooltip label="Delete Task" bg="red.600">
                    <IconButton
                      onClick={() => handleDeleteTask(task.id, colId)}
                      variant="ghost"
                      colorScheme="red"
                      aria-label="Delete Task"
                      px="2"
                      size="sm"
                      fontSize="18px"
                      icon={<DeleteIcon />}
                    />
                  </Tooltip>
                </div>
              </Editable>
              {provided.placeholder}
            </div>
          )}
        </Draggable>
      )

    })
  )


}


