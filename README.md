
# Task Management Board Application
### By Buseet. [Link to task 📢](https://bitbucket.org/buseet/frontend-tasks/src/master/)
--------------
![Preview Gif](./buseet.gif)

-------
## Features:
- ✅ As a user I can add a task to the todo column and only the todo column, with just text.
- ✅ As a user I can edit the task at any point to change its text.
- ✅ As a user I can delete the task at any point.
- ✅ As a user I can view the todo task with a popup box where I can view its title and description and edit the task.
- ✅ As a user I can move the task card between columns but with defined flow as such:
  - ✅ todo => in progress => done <br />
  - ✅ in progress => done or todo <br />
  - ✅ done => nowhere

- ✅ As a user I can see my tasks after I reload the page in their last state.

-----------
In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `yarn test`
Launches the test runner in the interactive watch mode.<br />

### `yarn build`
Builds the app for production to the `build` folder.<br />

### `yarn serve`
Runs the app in production mode, AFTER ```yarn build```.<br />
Follow these steps to view app :-

0. run ```yarn build``` -> to build app in /build folder.
1. run ```npm i -g serve``` -> to install serve globally.
2. run `yarn serve` -> to open server.
3. Open [http://localhost:5000](http://localhost:5000) -> to view it in the browser.

